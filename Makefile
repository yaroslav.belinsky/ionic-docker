#!/usr/bin/make

.PHONY : help run build

help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
---------------: ## ---------------

run: ## Start all containers (in background) for development
	docker run -d -p 8080:3000 ionic-go

build-go: ## Build all Docker images
	docker build -t ionic-go .

build-nginx: ## Build all Docker images
	docker build -t ionic-nginx -f Dockerfile.nginx .



