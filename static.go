package main
import (
  "net/http"
)

func main() {
	http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("./html"))))
	if err := http.ListenAndServe(":3000", nil); err != nil {
		panic(err)
	}
}