#Set of the initial image
FROM node:alpine AS angular-builder
#Defenition of the image description
LABEL Description="Ionic 4 application"
#Defenition of the image author
LABEL Maintainer="yaroslav.belinsky@gmail.com"
# Create app directory
WORKDIR /app
#copy of the package.json to the image
COPY package*.json ./
#run command inside container
RUN npm install -g @angular/cli && npm install
#copy sources to container
COPY . /app

#run build command
RUN [ "ng", "build", "--aot=true" ]


FROM golang:alpine AS go-builder

WORKDIR /app

COPY static.go static.go
COPY --from=angular-builder /app/www html

RUN CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    go build -a -tags netgo -ldflags '-w' -o /app/static

#make a release image from 
FROM scratch AS release

WORKDIR /app

COPY --from=go-builder /app/static /app/static
COPY --from=go-builder /app/html /app/html

ENTRYPOINT ["/app/static"]